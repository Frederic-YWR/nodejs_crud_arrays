"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.updateHostel = exports.replaceHotel = exports.deleteHostel = exports.addHostel = exports.getHostels = void 0;
var hostels_data_1 = require("../hostels.data");
function getHostels() {
    return hostels_data_1.hostelsData;
}
exports.getHostels = getHostels;
function addHostel(newHotel) {
    if (!newHotel) {
        throw new Error('new Hotel must be filled');
    }
    var hostels = getHostels();
    var id = hostels.length + 1;
    hostels.push(__assign({ id: id }, newHotel));
    return hostels;
}
exports.addHostel = addHostel;
function deleteHostel(hostelId) {
    var hostel = getHostels().filter(function (user) { return user.id !== hostelId; });
    var spread = getHostels().length - hostel.length;
    return (spread > 0 ? hostel : 'Cet hotel n\'existe pas');
}
exports.deleteHostel = deleteHostel;
function replaceHotel(hostelId, newHotel) {
    var id = hostelId;
    var hostels = getHostels();
    var index = hostels.findIndex(function (hostel) { return hostel.id === hostelId; });
    if (index == -1) {
        return "Désolé cet hotel n\'existe pas";
    }
    else {
        hostels[index] = __assign({ id: id }, newHotel);
        return hostels;
    }
}
exports.replaceHotel = replaceHotel;
function updateHostel(hostelId, toUpdate) {
    var id = hostelId;
    var hostels = getHostels();
    var index = hostels.findIndex(function (hostel) { return hostel.id === hostelId; });
    if (index == -1) {
        return "Désolé cet hotel n\'existe pas";
    }
    else {
        hostels[index] = __assign(__assign({}, hostels[index]), toUpdate);
        return hostels;
    }
}
exports.updateHostel = updateHostel;
var hotel = { name: 'hotel ocean' };

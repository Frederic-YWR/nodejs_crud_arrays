import {HotelModel} from '../models/hotel.model';
import {hostelsData} from '../hostels.data';
import {rooms} from '../hostels.data';




export function getHostels():HotelModel[] {
   
    return hostelsData
}

export function addHostel(newHotel:HotelModel):HotelModel[] {
      if(!newHotel) {
        throw new Error('new Hotel must be filled')
    }
    
  
    const hostels = getHostels();
    const id = hostels.length + 1;
    hostels.push({id,...newHotel});
    
   
    return hostels
}

export function deleteHostel(hostelId:number):HotelModel[] | string {


    const hostel = getHostels().filter((user)=> user.id !== hostelId);
    
    const spread = getHostels().length - hostel.length
    
    return (spread > 0 ? hostel : 'Cet hotel n\'existe pas')

}

export function replaceHotel(hostelId:number,newHotel:HotelModel) {
       
       const id = hostelId;
       const hostels = getHostels();
       const index = hostels.findIndex((hostel)=>hostel.id === hostelId);
       if (index == -1) {
       
           return  "Désolé cet hotel n\'existe pas"
           
       } else {
       
          hostels[index] = {id,...newHotel};
          
          return hostels
       }
}

export function updateHostel (hostelId:number,toUpdate:object) {
       
       const id = hostelId;
       const hostels = getHostels();
       const index = hostels.findIndex((hostel)=>hostel.id === hostelId);
       if (index == -1) {
       
           return  "Désolé cet hotel n\'existe pas"
           
       } else {
       
        hostels[index] = {...hostels[index],...toUpdate};
          
          return hostels
       }


}


    



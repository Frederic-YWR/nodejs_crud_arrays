import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {getHostels} from './services/hotel.service';
import {addHostel} from './services/hotel.service';
import {deleteHostel} from './services/hotel.service';
import {replaceHotel} from './services/hotel.service';
import {updateHostel} from './services/hotel.service';

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(cors());

app.get('/api', async (req, res) => {
    return res.send('coucou promo 4');
});

app.get('/api/hostels', async (req, res) => {
    
    return res.send(getHostels());
    
});


app.post('/api/hostels', async (req, res) => {
    
  
       const newHostel = req.body;
     
       return res.send(addHostel(newHostel));
    
});

app.delete('/api/hostels/:id', async (req, res) => {
    
  
       const HosteltoDelete = parseInt(req.params.id);
     
       return res.send(deleteHostel(HosteltoDelete));
    
});

app.put('/api/hostels/:id',(req,res)=> {
    
    const newHotel =req.body;
    const id = parseInt(req.params.id,10);
   
    return res.send(replaceHotel(id,newHotel))
});

app.patch('/api/hostels/:id',(req,res)=> {
    
    const to_update =req.body;
    const id = parseInt(req.params.id,10);
   
    return res.send(updateHostel(id,to_update))
});





app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});



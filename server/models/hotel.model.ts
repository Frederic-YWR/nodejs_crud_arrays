import {RoomModel} from './room.model';


export interface HotelModel {
    id?: number | string;
    name: string;
    roomNumbers: number;
    pool: boolean;
    rooms: number[];
}

